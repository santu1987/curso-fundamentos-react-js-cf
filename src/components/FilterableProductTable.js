import React , {Component} from 'react';
import SearchBar from './SearchBar.js';
import ProductTable from './ProductTable.js';

class FilterableProductTable extends Component{
	constructor(){
		super()
		this.state = {
			filter: null
		}
	}
	
	//--Cuerpo de metodos
	filterList(ev){
		let filter = ev.target.value
		this.setState({
			filter: filter
		})
	}
	//--
	//Cuerpo de render
	render(){
		//--
		
		//--
		return(
			<div>
				<SearchBar onChange={this.filterList.bind(this)}/>
				<ProductTable products={this.props.store} filter={ this.state.filter} />
			</div>
		);
	}
}
export default FilterableProductTable;
/*
1-Con el bin amarra la función a los componentes sino serviria solo para el componente SearchBar
*/