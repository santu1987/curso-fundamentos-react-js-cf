import React , {Component} from 'react';
import ProductCategoryRow from './ProductCategoryRow.js'
import ProductRow from './ProductRow.js'

class ProductTable  extends Component{
	render(){

		let rows = []
		let last_category = null
		//---
		if((this.props.filter == null) && (this.props.products != null)){
			this.props.products.forEach((product)=>{
				//--Debo agregar un categoryrow si no hay otro producto de la categoria anterior
				if(product.category != last_category)
					rows.push(<ProductCategoryRow category={product.category} key={product.category}/>);
				//else
				rows.push(<ProductRow product={product.name} key={product.name}/>)
				last_category = product.category
			})
		}else if((this.props.filter != null) && (this.props.products != null)){
			this.props.products.forEach((product)=>{
				
				let filter = this.props.filter
				if(product.name.indexOf(filter)>-1){
					//-------------------------------------
					//--Debo agregar un categoryrow si no hay otro producto de la categoria anterior
					if(product.category != last_category)
						rows.push(<ProductCategoryRow category={product.category} key={product.category}/>);
					//else
						rows.push(<ProductRow product={product.name} key={product.name}/>)
						last_category = product.category
					//-------------------------------------
				}
			})
		}
		else{
			rows.push(<h1>Loading</h1>);
		}
		//---
		return(
			<div>
				{rows}
			</div>
		);
	}
}

export default ProductTable;