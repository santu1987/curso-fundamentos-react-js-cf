import React , {Component} from 'react';

class ProductRow extends Component{
	render(){
		return(
			<h6>{ this.props.product }</h6>
		);
	}
}

export default ProductRow;