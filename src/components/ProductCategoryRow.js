import React , {Component} from 'react';

class ProductCategoryRow extends Component{
	render(){
		return(
			<h4>{ this.props.category }</h4>
		);
	}
}

export default ProductCategoryRow;