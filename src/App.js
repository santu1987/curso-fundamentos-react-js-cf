import React from 'react';
import logo from './logo.svg';
import './assets/css/App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
/*
1-BrowserRouter que es un enrutador que utiliza la API de history de HTML5 para mantener sincronizada la UI con la ruta que toque.
2-Switch que es para que solamente renderice el primer componente que coincida con la ruta. 
3-Route que es el componente que define las rutas ( path) y el componente que debe renderizar ( component)
*/
import ComponenteInicial from './components/ComponenteInicial';
import FilterableProductTable from './components/FilterableProductTable';
import Home from './pages/Home';
import Store from './pages/Store';
/*const db= [
            {category: "Sporting Goods", price:"$49.99", stocked: true, name:"Football"},
            {category: "Sporting Goods", price:"$9.99", stocked: true, name:"Baseball"},
            {category: "Sporting Goods", price:"$29.99", stocked: true, name:"Basketball"},
            {category: "Electronics", price:"$99.99", stocked: true, name:"Ipod Touch"},
            {category: "Electronics", price:"$399.99", stocked: true, name:"Iphone 5"},
            {category: "Electronics", price:"$199.99", stocked: true, name:"Nexus 7"}

          ];*/


function App() {
  return (
    <div className="App">
      <header className="">
      </header>
      <section className="components">
        <Router>
          <div>
            <Route path="/" component={Home} />
            <Route path="/store" component={Store} />  
          </div>   
        </Router>
      </section>
    </div>
  );
}
/*<FilterableProductTable  store={db} />*/
export default App;
