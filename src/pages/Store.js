import React , {Component} from 'react';
import FilterableProductTable from '../components/FilterableProductTable';



class Store extends Component{
	constructor(){
		super()
	}

	render(){
		//----
		const db= [
            {category: "Sporting Goods", price:"$49.99", stocked: true, name:"Football"},
            {category: "Sporting Goods", price:"$9.99", stocked: true, name:"Baseball"},
            {category: "Sporting Goods", price:"$29.99", stocked: true, name:"Basketball"},
            {category: "Electronics", price:"$99.99", stocked: true, name:"Ipod Touch"},
            {category: "Electronics", price:"$399.99", stocked: true, name:"Iphone 5"},
            {category: "Electronics", price:"$199.99", stocked: true, name:"Nexus 7"}

        ];
		//----
		return(
			<div>
				<FilterableProductTable  store={db} />
			</div>
		);
	}
}

export default Store;