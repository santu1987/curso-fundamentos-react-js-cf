import React , {Component} from 'react';
import {Link} from 'react-router-dom';


class Home extends Component{
	
	constructor(){
		super()
	}

	render(){
		return(
			<div>
				<h1>Home</h1>
				<Link to="/store">Store</Link>
			</div>
		);
	}
}

export default Home;